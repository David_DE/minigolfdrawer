<?php
ob_start();
header('Content-Type: text/html;charset=utf-8');
include("php/functions.php");
?>
<!DOCTYPE html>
<html>
<head>
    <title>Minigolf-Drawer</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <script language="javascript" type="text/javascript" src="js/main.js"></script>
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
	<link href="css/design.css" rel="stylesheet">
</head>
<body>
    <?php
    error_reporting(-1);
    function __autoload($class_name) {
        include 'php/' . $class_name . '.php';
    }

    $mini = new Minigolf();
    ?>

    <div class="navbar">
        <div class="navbar-inner">
            <div class="container">

                <!-- .btn-navbar is used as the toggle for collapsed navbar content -->
                <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>

                <!-- Be sure to leave the brand out there if you want it shown -->
                <a class="brand" href="index.php">Minigolf-Drawer</a>

                <!-- Everything you want hidden at 940px or less, place within here -->
                <div class="nav-collapse collapse">
                    <ul class="nav">
                    	<li
                        <?php if (!isset($_GET["anlage"])) {
							echo ' class="active"';
						}
						?>
                        ><a href="index.php">Home</a></li>
                        <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Anlagen<b class="caret"></b></a>
                        	<ul id="navi_anlagen_dropdown" class="dropdown-menu">
                        		<?php
                            		$mini->getAlleAnlagenFuerNavi();
                        		?>
                        	</ul>
                        </li>
                     	<?php
                        	if (isset($_GET["anlage"]) && $mini->checkForAnlage($_GET["anlage"]) && $mini->anlageHatBahn($_GET["anlage"])) {
                            	echo '<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">Bahnen<b class="caret"></b></a>';
                            	echo '<ul id="navi_bahnen_dropdown" class="dropdown-menu">';
                            	$mini->getAlleBahnenFuerNavi($_GET["anlage"]);
                            	echo '</ul>';
                             }
                        ?>
                        </li>
                       <?php
                            if (isset($_GET["anlage"]) && isset($_GET["bahn"]) && $mini->checkForBahn($_GET["anlage"], $_GET["bahn"]) && $mini->bahnHatWege($_GET["anlage"], $_GET["bahn"])) {
                            	echo '<li class="dropdown" class="active">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">Ballwege<b class="caret"></b></a>';
                                echo '<ul id="navi_bahnen_dropdown" class="dropdown-menu">';
                                $mini->getAlleBallwegeFuerNavi($_GET["anlage"], $_GET["bahn"]);
                                echo '</ul>';
                             }
                        ?>
                        <li><a href="help.php">Hilfe</a></li>
                        <li><a href="contact.php">Kontakt</a></li>
                    </ul>
                    <?php 
					if (isset($_SESSION['username']) && ($_SESSION['ID']) ) {
		    			if (isset($_COOKIE['username']) && isset($_COOKIE['ID'])) {
                			$_SESSION['username']=$_COOKIE['username'];
							$_SESSION['ID']=$_COOKIE['ID'];
							
			           		echo '<div class="navbar-form pull-right">Herzlich Willkommen <a href="profil.php">'
							.$_SESSION['username'].'</a> ! <a href="?type=logout">Logout</a></div>';
						
            			} else {
                			setcookie("username", "", time()-3600);
							setcookie("ID", "", time()-3600);
            		
							echo '
							<form class="navbar-form pull-right" method="post">
							<input name="username_login" class="span2" type="text" placeholder="Username">
							<input name="password_login" class="span2" type="password" placeholder="Password">
							<button name="submit_login" class="btn" type="submit">Login</button>
							<button name="submit_regist" class="btn" type="submit"><Registrieren</button>
							</form>
							'
							;
							
						}
					} else {
						echo '
							<form class="navbar-form pull-right" method="post">
							<input name="username_login" class="span2" type="text" placeholder="Username">
							<input name="password_login" class="span2" type="password" placeholder="Password">
							<button name="submit_login" class="btn" type="submit">Login</button>
							<button name="submit_regist" class="btn" type="submit">Registrieren</button>	
							</form>'
							;	
					}
					?> 	
                </div>
            </div>
        </div>
    </div>
	
    <div class="container">
    <div id="error">
    <?php
        if (isset($_POST["error"])) {
            echo $_POST["error"];
        }

    ?>
    </div>

        <!--<script type="text/javascript">errorMessage("Beide Felder ausfüllen");</script>-->
    	<div id="content">

       	<?php 
		if (isset($_GET["anlage"]) && isset($_GET["bahn"]) && isset($_GET["weg"])) {
            if ($mini->checkForWeg($_GET["anlage"], $_GET["bahn"], $_GET["weg"])) {

			?>
        <canvas id="canvas" width="500px" height="500px">
        </canvas>
        <script type="text/javascript">
  	         window.onload = function() {
                test_draw();
            }

            window.onresize = function(event) {
                test_draw();
            }

            function test_draw() {
                var canvas = document.getElementById('canvas');
                var context = canvas.getContext('2d');
                fitCanvasToResulution();
				drawMinigolfBahn(<?php echo $mini->getBahnTyp($_GET["anlage"], $_GET["bahn"]); ?>);

                <?php
                    $mini->getBallWegeKoords($_GET["anlage"], $_GET["bahn"], $_GET["weg"]);
                ?>
            }

        </script>

        <?php
			
			
			$ballwege = $mini->getBallWegeInfo($_GET["anlage"], $_GET["bahn"], $_GET["weg"]);
			echo 
			'<div class="uebersicht">
			<table cellpadding="5px">
			<tr>
			<td colspan="3"><b>Ballweginfo</b></td>
			</tr>
			<tr>
			<td valign="top">Anlage:</td>
			<td></td>
			<td>' .$ballwege[0] . '</td>
			</tr>
			<tr height="3px"></tr>
			<tr>
			<td valign="top">Bahn:</td>
			<td></td>
			<td>' .$ballwege[1] . '</td>
			</tr>
			<tr height="3px"></tr>
			<tr>
			<td valign="top">Ballweg:</td>
			<td></td>
			<td>' .$ballwege[2] . '</td>
			</tr>
			<tr height="3px"></tr>
			<tr>
			<td valign="top">Autor:</td>
			<td></td>
			<td>' .$ballwege[3] . '</td>
			</tr>
			<tr height="3px"></tr>
			<tr>
			<td valign="top">Beschreibung:</td>
			<td></td>
			<td>' .$ballwege[4] . '</td>
			</tr>
			<tr height="3px"></tr>
			<tr>
			<td valign="top">Ballart:</td>
			<td></td>
			<td>' .$ballwege[5] . '</td>
			</tr>
			</table>
			</div>
			<br>
			
			<form method="post">
            <table>
                <tr>
                    <td>
            			<textarea class="span2 kommentarfeld_gross" placeholder="Ihr Kommentar..." name="kommentar"></textarea><br>
					</td>
				</tr>
                <tr>
                	<td>                                           
			            <button type="submit" name="submit_kommentar" class="btn">Absenden</button>
            		</td>
				</tr>
			</table>                	                   
            </form>
			<br>';
			printKommi();
        } else {
            echo 'Anlage, Bahn oder Ballweg nicht gefunden!';
        }

		} else if (isset($_GET["anlage"]) && isset($_GET["bahn"])) {
            if ($mini->checkForBahn($_GET["anlage"], $_GET["bahn"])) {

            //}
		?>
        <canvas id="canvas" width="500px" height="500px">
        </canvas>
        <div class="btn-group">
				<button type="button" id="btn_draw" class="btn">Einzeichnen</button>
                <button type="button" id="btn_save" class="btn hidden">Speichern</button>
				<button type="button" id="btn_reset" class="btn hidden">Reset</button>
                <button type="button" id="btn_stop" class="btn hidden">Einzeichnen stoppen</button>
		</div>
        <br><br>

        <div id="weg_speichern_formular">
	    Kommentar zum Ballweg:<br />
		<textarea class="span2 kommentarfeld" id ="weg_kommentar" placeholder="Ihr Kommentar..." name="kommentar"></textarea>
		<br />Ballart:<br />

            <?php
                $mini->printBallArtenDropdown();
            ?>
        </div>

        <script type="text/javascript">

            var koords_array = new Array();

  	         window.onload = function() {
                test_draw();


                 $("#btn_draw").on('click', function(event) {

                     $("#btn_save").removeClass("hidden");
                     $("#btn_reset").removeClass("hidden");
                     $("#btn_stop").removeClass("hidden");

                     setCanvasClick();

                     $("#btn_save").on('click', function(event) {
                         if (saveCanvas(<?php (isset($_SESSION['ID'])) ? $ses=$_SESSION['ID'] : $ses=0; printf ("%d, %d, %d", $_GET["anlage"], $_GET["bahn"], $ses); ?>)) {
                             $("#btn_save").addClass("hidden");
                             $("#btn_reset").addClass("hidden");
                             $("#btn_stop").addClass("hidden");
                             $("#weg_kommentar").val("");
                             //window.location.reload();
                         }

                     });

                     $("#btn_reset").on('click', function(event) {
                         resetCanvas(<?php echo $mini->getBahnTyp($_GET["anlage"], $_GET["bahn"]); ?>);
                         errorMessage("");
                     });

                     $("#btn_stop").on('click', function(event) {
                         resetCanvas(<?php echo $mini->getBahnTyp($_GET["anlage"], $_GET["bahn"]); ?>);

                         $("#btn_save").addClass("hidden");
                         $("#btn_reset").addClass("hidden");
                         $("#btn_stop").addClass("hidden");

                         $("#btn_reset").unbind();
                         $("#btn_save").unbind();
                         $('canvas').unbind();
                     });
                 });


            }

            window.onresize = function(event) {
                test_draw();
            }

            function test_draw() {
                var canvas = document.getElementById('canvas');
                var context = canvas.getContext('2d');
                fitCanvasToResulution();
				drawMinigolfBahn(<?php echo $mini->getBahnTyp($_GET["anlage"], $_GET["bahn"]); ?>);
                drawLinesWhileDrawing();

            }

        </script>
        <?php
            //if ($mini->checkForBahn($_GET["anlage"], $_GET["bahn"])) {
                $bahninfo = $mini->getBahnInfo($_GET["anlage"], $_GET["bahn"]);
                echo '<div class="uebersicht">
				<table cellpadding="5px">
				<tr>
				<td valign="top">Anlage:</td>
				<td></td>
				<td>' . $bahninfo[0] .'</td>
				</tr>
				<tr height="3px"></tr>
				<tr>
				<td valign="top">Bahnnummer:</td>
				<td></td>
				<td>' . $bahninfo[1] . '</td>
				</tr>
				<tr height="3px"></tr>
				<tr>
				<td valign="top">Bahnnahme:</td>
				<td></td>
				<td>' . $bahninfo[2] . '</td>
				</tr>
				</table>

			</div>';
            } else {
                echo 'Anlage oder Bahn nicht gefunden!';
            }

		} else if (isset($_GET["anlage"])) {
            if ($mini->checkForAnlage($_GET["anlage"])) {
                $anlagen = $mini->getAnlagenInfo($_GET["anlage"]);
                echo '<div class="uebersicht">
				<table cellpadding="5px">
				<tr>
				<td colspan="3"><b>' . $anlagen[0] . '</b></td>
				</tr>
				<tr height="3px"></tr>
				<tr>
				<td valign="top">Name:</td>
				<td></td>
				<td>' . $anlagen[1] . '</td>
				</tr>
				<tr height="3px"></tr>
				<tr>
				<td valign="top">Ort:</td>
				<td></td>
				<td>' . $anlagen[2] . '</td>
				</tr>
				<tr height="3px"></tr>
				<tr>
				<td valign="top">PLZ:</td>
				<td></td>
				<td>' . $anlagen[3] . '</td>
				</tr>
				<tr height="3px"></tr>
				<tr>
				<td valign="top">Straße:</td>
				<td></td>
				<td>' . $anlagen[4] . '</td>
				</tr>
				</table>
				</div>
				<br>
				';
            } else {
                echo 'Anlage nicht gefunden!';
            }
		} else {
			echo'
            Ein Herzliches Willkommen an alle Minigolf-Fans! <br><br>
           	Unsere Homepage ist dazu da, Ihnen das Minigolfspielen zu erleichtern! <br>
			W&auml;len Sie einfach eine Anlage und die jeweilige Bahn aus, auf der Sie spielen möchten und los gehts! <br>
			Sie k&ouml;nnen sich den optimalen Ballweg der Bahn, sowie weitere Infos wie z.B. Ballart anzeigen lassen.
			Wenn Sie eingeloggt sind k&ouml;nnen Sie selbst sogar drauf loszeichnen, um Anderen zu helfen und sie an Ihrem Wissen teilhaben zu lassen. Einfach die jeweilige Bahn ausw&auml;hlen und auf Zeichnen dr&uuml;cken.<br>
			Haben Sie viel Spaß!
			<br>
			Wir hoffen Sehr, dass Ihnen unsere Homepage hilft. F&uuml;r Fragen und Anregungen steht Ihnen das <a href="contact.php">Kontaktformular</a> zur Verf&uuml;gung. 
			<br><br>
			Ihr Minigolf-Drawer Team
			<br><br>
            <img src="img/Minigolf.jpg" class="abgerundet" alt="Minigolfschläger">
			';
		 } 


    		
			?>
     	</div>
        <div id="footer">
   			&copy; David Frost Christian Sch&auml;fer Laura Unger
        </div>
	</div>
	

    <!-- /container -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="http://code.jquery.com/jquery.js"></script>
    <script src="js/bootstrap.js"></script>

    <script language="javascript" type="text/javascript" src="js/draw.js"></script>
    <script language="javascript" type="text/javascript" src="js/canvas.js"></script>

</body>
</html>