<?php
ob_start();
header('Content-Type: text/html;charset=utf-8');
include("php/functions.php");
?>
<!DOCTYPE html>
<html>
<head>
    <title>Minigolf-Drawer</title>

    <!--<meta name="viewport" content="width=device-width, initial-scale=1.0">-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
	<link href="css/design.css" rel="stylesheet">
    <script language="javascript" type="text/javascript" src="js/main.js"></script>
    <script language="javascript" type="text/javascript" src="js/draw.js"></script>
    <script language="javascript" type="text/javascript" src="js/canvas.js"></script>

</head>
<body>
    <?php
    error_reporting(-1);
    function __autoload($class_name) {
        include 'php/' . $class_name . '.php';
    }

    $mini = new Minigolf();
    ?>
    <div class="navbar">
        <div class="navbar-inner">
            <div class="container">

                <!-- .btn-navbar is used as the toggle for collapsed navbar content -->
                <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>

                <!-- Be sure to leave the brand out there if you want it shown -->
                <a class="brand" href="index.php">Minigolf-Drawer</a>

                <!-- Everything you want hidden at 940px or less, place within here -->
                <div class="nav-collapse collapse">
                    <ul class="nav">
                    	<li><a href="index.php">Home</a></li>
                        <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Anlagen<b class="caret"></b></a>
                        	<ul id="navi_anlagen_dropdown" class="dropdown-menu">
                        		<?php
                            		$mini->getAlleAnlagenFuerNavi();
                        		?>
                        	</ul>
                        </li>
                     	<?php
                        	if (isset($_GET["anlage"])) {
                            	echo '<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">Bahnen<b class="caret"></b></a>';
                            	echo '<ul id="navi_bahnen_dropdown" class="dropdown-menu">';
                            	$mini->getAlleBahnenFuerNavi($_GET["anlage"]);
                            	echo '</ul>';
                             }
                        ?>
                        </li>
                       <?php
                            if (isset($_GET["anlage"]) && isset($_GET["bahn"])) {
                            	echo '<li class="dropdown" class="active">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">Ballwege<b class="caret"></b></a>';
                                echo '<ul id="navi_bahnen_dropdown" class="dropdown-menu">';
                                $mini->getAlleBallwegeFuerNavi($_GET["anlage"], $_GET["bahn"]); //$_GET["bahn"]
                                echo '</ul>';
                             }
                        ?>
                        </li>
                        <li><a href="help.php">Hilfe</a></li>
                        <li><a href="contact.php">Kontakt</a></li>
                    </ul>
                    <?php 
					if (isset($_SESSION['username']) && ($_SESSION['ID']) ) {
		    			if (isset($_COOKIE['username']) && isset($_COOKIE['ID'])) {
                			$_SESSION['username']=$_COOKIE['username'];
							$_SESSION['ID']=$_COOKIE['ID'];
							
			           		echo '<div class="navbar-form pull-right">Herzlich Willkommen <a href="profil.php">'
							.$_SESSION['username'].'</a> ! <a href="?type=logout">Logout</a></div>';
						
            			} else {
                			setcookie("username", "", time()-3600);
							setcookie("ID", "", time()-3600);
            		
							echo '
							<form class="navbar-form pull-right" method="post" action="">
							<input name="username_login" class="span2" type="text" placeholder="Username">
							<input name="password_login" class="span2" type="password" placeholder="Password">
							<button name="submit_login" class="btn" type="submit">Login</button>
							<button name="submit_regist" class="btn" type="submit">Registrieren</button>			
							</form>';
							
						}
					} else {
						echo '
							<form class="navbar-form pull-right" method="post" action="">
							<input name="username_login" class="span2" type="text" placeholder="Username">
							<input name="password_login" class="span2" type="password" placeholder="Password">
							<button name="submit_login" class="btn-primary" type="submit">Login</button>
							<button name="submit_regist" class="btn" type="submit">Registrieren</button>			
							</form>';	
					}
					?> 	
                </div>
            </div>
        </div>
    </div>
    <div class="container">
    <div id="error">
    <?php
        if (isset($_POST["error"])) {
            echo $_POST["error"];
        }

    ?>
    </div>
    	<div id="content">
        	<h3>Profil bearbeiten</h3>
           
				<p><b>Namen ändern</b></p>	
                <?php $user_detail = $mini->getUserDetail($_SESSION['ID']); 
                echo '
                <form method="post">

                    <label for="vorname">Vorname</label>
                        <input id="vorname" name="vorname_aendern" size="25" type="text" value="'.$user_detail[0].'" />
    
                    <label for="nachame">Nachname</label>
                        <input id="nachname" name="nachname_aendern" size="25" type="text" value="'.$user_detail[1].'" />
					    <br>
                    <input id="submit" class="btn" name="submit_name_aendern" type="submit" value="Name ändern" />
                </form>	
                <br>
                <p><b>Passwort ändern</b></p>
                <form method="post">

                    <label for="aktuelles_pw">Aktuelles Passwort</label>
                        <input id="aktuelles_pw" name="aktuelles_pw" size="25" type="password" />
    
                    <label for="neues_pw1">Neues Passwort</label>
                        <input id="neues_pw1" name="neues_pw1" size="25" type="password" />
					
                    <label for="neues_pw2">Neues Passwort bestätigen</label>
                        <input id="neues_pw2" name="neues_pw2" size="25" type="password" />

    				<br>
                    <input id="submit" class="btn" name="submit_pw_aendern" type="submit" value="Passwort ändern" />
                </form>';

				?>
                <h3>Anlage hinzufügen</h3>
                <form method="post">
                <label for="name_anlage">Name</label>
                <input id="name_anlage" name="name_anlage" size="25" type="text" />
    
                <label for="beschreibung">Beschreibung</label>
                <input id="beschreibung" name="beschreibung" size="25" type="text" />
				
                <label for="strasse">Straße</label>
                <input id="strasse" name="strasse" size="25" type="text" />

                <label for="plz">PLZ</label>
                <input id="plz" name="plz" size="25" type="text" />
                
                <label for="ort">Ort</label>
                <input id="ort" name="ort" size="25" type="text" />

   				<br>
                <input id="submit" class="btn" name="submit_anlage" type="submit" value="Hinzufügen" />
                </form>
                
                 <h3>Bahn hinzufügen</h3>
                <form method="post">
                <label for="anlagen_id">Anlage</label>
               	<?php 
				$id = $_SESSION['ID'];
				$mini->printOwnAnlagenDropdown($id);
    			?>
                <label for="name_bahn">Bahnname</label>
                <input id="name_bahn" name="name_bahn" size="25" type="text" />
                
                <label for="nummer_bahn">Bahnnummer</label>
                <input id="nummer_bahn" name="nummer_bahn" size="25" type="text" />
                <br>
                <label for="bahntyp">Bahntyp</label>
                <select name="bahntyp">
	                <option value="0">0</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                </select>

   				<br>
                <input id="submit" class="btn" name="submit_bahn" type="submit" value="Hinzufügen" />
                </form>

        </div>
        <div id="footer">
   			&copy; David Frost Christian Sch&auml;fer Laura Unger
        </div>
     </div>
    <script src="http://code.jquery.com/jquery.js"></script>
    <script src="js/bootstrap.js"></script>
</html> 