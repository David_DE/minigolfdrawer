﻿<?php

class Minigolf {
    private $mysqli;

    public function __construct() {
        $this->mysqli = new mysqli("localhost", "USER", "PASSWORD", "db_mini");
        if ($this->mysqli->connect_errno) {
            echo "Fehler bei Verbindungsaufbau!: ".$this->mysqli->connect_errno;
        }
        mysqli_query($this->mysqli, "SET NAMES 'utf8'");
    }

    // ----------------------------------------- Navigation print Funktionen -----------------------------------------
    /*
     * Gibt per echo Listenelemente fuer Anlagen Navigations Dropdown aus
     */
    public function getAlleAnlagenFuerNavi() {
        if (!($stmt = $this->mysqli->prepare('SELECT AnlageId, Name, Ort FROM Anlage'))) {
            echo "Prepare failed: (" . $this->mysqli->errno . ") " . $this->mysqli->error;
        }

        /*
        if (!$stmt->bind_param("i", $anlage)) {
            echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
        }*/

        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }

        $stmt->store_result();

        $ort = '';
        $stmt->bind_result($anlage, $name, $ort);

        while($stmt->fetch()) {
            echo '<li><a href="index.php?anlage=' . $anlage . '">' . $ort . ' - ' . $name . '</a></li>';
        }

    }

    /*
     * Gibt per echo Listenelemente fuer Bahnen Navigations Dropdown aus
     */
    public function getAlleBahnenFuerNavi($anlage) {
        $anlage = mysqli_real_escape_string($this->mysqli, $anlage);

        if (!($stmt = $this->mysqli->prepare('SELECT BahnNummer, Name FROM Bahn WHERE Anlage=?'))) {
            echo "Prepare failed: (" . $this->mysqli->errno . ") " . $this->mysqli->error;
        }

        if (!$stmt->bind_param("i", $anlage)) {
            echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
        }

        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }

        $stmt->bind_result($nummer, $name);

        while($stmt->fetch()) {
            echo '<li><a href="index.php?anlage=' . $anlage . '&bahn=' . $nummer . '"> Bahn ' . $nummer . ' - ' . $name . '</a></li>';
        }

    }

    /*
     * Gibt per echo Listenelemente fuer Ballwege Navigations Dropdown aus
     */
    public function getAlleBallwegeFuerNavi($anlage, $bahnNummer) {
        $anlage = mysqli_real_escape_string($this->mysqli, $anlage);
        $bahnNummer = mysqli_real_escape_string($this->mysqli, $bahnNummer);

        if (!($stmt = $this->mysqli->prepare('SELECT WegNummer, Username FROM Ballweg, User WHERE Ballweg.Name = User.Userid AND Anlage=? AND Bahn=?'))) {
            echo "Prepare failed: (" . $this->mysqli->errno . ") " . $this->mysqli->error;
        }

        if (!$stmt->bind_param("ii", $anlage, $bahnNummer)) {
            echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
        }

        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }

        $stmt->store_result();

        if ($stmt->num_rows == 0) {
            $this->errorMessage('Fehler beim auslesen! Anlage'.$anlage.' mit Bahn '.$bahnNummer.' nicht gefunden!');
            return;
        }

        $stmt->bind_result($wegeNummer, $name);

        while($stmt->fetch()) {
            echo '<li><a href="index.php?anlage=' . $anlage . '&bahn=' . $bahnNummer . '&weg=' . $wegeNummer . '"> Ballweg von ' . $name . '</a></li>';
        }
    }

    // ----------------------------------------- get Info Funktionen -----------------------------------------
    /*
     * Gibt array mit Infos zu einer Anlage als Array zurueck:
     * [0] -> beschreibung, [1] -> name, [2] -> ort, [3] -> plz, [4] -> strasse
     */
    public function getAnlagenInfo($anlage) {
        $anlage = mysqli_real_escape_string($this->mysqli, $anlage);

        if (!($stmt = $this->mysqli->prepare('SELECT Beschreibung, Name, Ort, Plz, Strasse FROM Anlage WHERE AnlageId=?'))) {
            echo "Prepare failed: (" . $this->mysqli->errno . ") " . $this->mysqli->error;
        }


        if (!$stmt->bind_param("i", $anlage)) {
            echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
        }

        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }

        $ort = ''; $plz=''; $strasse='';
        $stmt->bind_result($beschreibung, $name, $ort, $plz, $strasse);

        $stmt->fetch();

        $array = array($beschreibung, $name, $ort, $plz, $strasse);

        return $array;
    }

    /*
     * Gibt array mit Infos zu eine Bahn als Array zurueck:
     * [0] -> anlagen Name, [1] -> bahnId, [2] -> bahnName
     */
    public function getBahnInfo($anlage, $bahn) {
        $anlage = mysqli_real_escape_string($this->mysqli, $anlage);
        $bahn = mysqli_real_escape_string($this->mysqli, $bahn);

        //if (!($stmt = $this->mysqli->prepare('SELECT Anlage.Name, BahnNummer, Bahn.Name, FROM Bahn, Anlage WHERE Anlage.AnlageId=Bahn.Anlage AND Bahn.Anlage=? AND Bahn.BahnNummer=?'))) {
        if (!($stmt = $this->mysqli->prepare('SELECT Anlage.Name, Bahn.BahnNummer, Bahn.Name FROM Anlage, Bahn WHERE AnlageId=? AND BahnNummer=? AND Anlage = AnlageId'))) {
            echo "Prepare failed: (" . $this->mysqli->errno . ") " . $this->mysqli->error;
        }


        if (!$stmt->bind_param("ii", $anlage, $bahn)) {
            echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
        }

        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }

        $bahnName='';
        $stmt->bind_result($anlagenname, $bahnNummer, $bahnName);

        $stmt->fetch();

        $array = array($anlagenname, $bahnNummer, $bahnName);

        return $array;
    }

    /*
     * Gibt array mit Infos zu einem Ballweg als Array zurueck:
     * [0] -> anlage, [1] -> bahn, [2] -> weg, [3] -> autor, [4] -> kommentar,[5] -> ballart
     */
    public function  getBallWegeInfo($anlage, $bahnNummer, $wegNummer) {
        $anlage = mysqli_real_escape_string($this->mysqli, $anlage);
        $bahnNummer = mysqli_real_escape_string($this->mysqli, $bahnNummer);
        $wegNummer = mysqli_real_escape_string($this->mysqli, $wegNummer);

        if (!($stmt = $this->mysqli->prepare('SELECT a.Name, u.Username, Kommentar, ba.Name FROM Ballweg as bw, BallArt as ba, Anlage as a , User as u WHERE u.UserId = bw.Name AND bw.Anlage = a.AnlageId AND bw.BallArt=ba.BallId AND Anlage=? AND Bahn=? AND WegNummer=?'))) {
            echo "Prepare failed: (" . $this->mysqli->errno . ") " . $this->mysqli->error;
        }

        if (!$stmt->bind_param("iii", $anlage, $bahnNummer, $wegNummer)) {
            echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
        }

        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }

        $ballart=''; $kommentar=''; $autor='';
        $stmt->bind_result($anlageName, $autor, $kommentar, $ballart);

        $stmt->fetch();

        return $ballweg = array($anlageName, $bahnNummer, $wegNummer, $autor, $kommentar, $ballart);
    }


    public function insertAnlage($name, $beschreibung, $ort, $plz, $strasse) {
        $name = mysqli_real_escape_string($this->mysqli, $name);
        $beschreibung = mysqli_real_escape_string($this->mysqli, $beschreibung);
        $ort = mysqli_real_escape_string($this->mysqli, $ort);
        $plz = mysqli_real_escape_string($this->mysqli, $plz);
        $strasse = mysqli_real_escape_string($this->mysqli, $strasse);

        if (!($stmt = $this->mysqli->prepare("INSERT INTO Anlage(Name, Beschreibung, Ort, Plz, Strasse) VALUES(?, ?, ?, ?, ?)"))) {
            echo "Prepare failed: (" . $this->mysqli->errno . ") " . $this->mysqli->error;
            return 0;
        }

        if (!$stmt->bind_param('sssss', $name, $beschreibung, $ort, $plz, $strasse)) {
            echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
            return 0;
        }

        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            return 0;
        }


        if ($stmt->affected_rows != 1) {
            echo "Fehler in insertanlage()";
            return 0;
        }

        return $this->mysqli->insert_id;
    }


    // ----------------------------------------- User Funktionen -----------------------------------------
    /*
     * Gibt true zurueck, falls uebergebenes Passwort mit Passwort des uebergebenen Users uebereinstimmt
     */
    public function validateUserPw($user, $password) {
        $user = mysqli_real_escape_string($this->mysqli, $user);

        if (!($stmt = $this->mysqli->prepare("SELECT Passwort FROM User WHERE Username=?"))) {
            echo "Prepare failed: (" . $this->mysqli->errno . ") " . $this->mysqli->error;
        }

        if (!$stmt->bind_param("s", $user)) {
            echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
        }

        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }

        $stmt->bind_result($passw);
        $stmt->fetch();

        if ($password == $passw) {
            return true;
        }

        return false;
    }

    /*
     * Gibt die Userid eines Usernamens zurück. Falls Id = 0, dann gibt es den User nicht.
     */
    public function getUserId($username) {
        $username = mysqli_real_escape_string($this->mysqli, $username);

        if (!($stmt = $this->mysqli->prepare('SELECT UserId FROM User WHERE Username=?'))) {
            echo "Prepare failed: (" . $this->mysqli->errno . ") " . $this->mysqli->error;
        }

        if (!$stmt->bind_param("s", $username)) {
            echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
        }

        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }

        $stmt->bind_result($userId);
        $stmt->fetch();

        return $userId;
    }


    public function insertUser($username, $passwort, $vorname, $nachname) {
        $username = mysqli_real_escape_string($this->mysqli, $username);
        $passwort = mysqli_real_escape_string($this->mysqli, $passwort);
        $vorname = mysqli_real_escape_string($this->mysqli, $vorname);
        $nachname = mysqli_real_escape_string($this->mysqli, $nachname);

        if (!$stmt = $this->mysqli->prepare("INSERT INTO User(Username, Passwort, Vorname, Nachname) VALUES(?, ?, ?, ?)")) {
            echo "Prepare failed: (" . $this->mysqli->errno . ") " . $this->mysqli->error;
            return false;
        }

        if (!$stmt->bind_param('ssss', $username, $passwort, $vorname, $nachname)) {
            echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
            return false;
        }
        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            return false;
        }

        if ($stmt->affected_rows != 1) {
            return false;
        }

        return true;
    }

    /*
     *
     */
    public function updateUserDetails($uid, $vorname, $nachname) {
        $uid = mysqli_real_escape_string($this->mysqli, $uid);
        $vorname = mysqli_real_escape_string($this->mysqli, $vorname);
        $nachname = mysqli_real_escape_string($this->mysqli, $nachname);

        if (!$stmt = $this->mysqli->prepare("UPDATE User SET Vorname=?, Nachname=? WHERE UserId=?")) {
            echo "Prepare failed: (" . $this->mysqli->errno . ") " . $this->mysqli->error;
            return false;
        }

        if (!$stmt->bind_param('sss', $vorname, $nachname, $uid)) {
            echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
            return false;
        }
        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            return false;
        }

        if ($stmt->affected_rows != 1) {
            return false;
        }

        return true;
    }

    /*
     * Setzt das Userpasswort auf uebergebenen Wert
     */
    public function updateUserPassword($uid, $newPw) {
        $uid = mysqli_real_escape_string($this->mysqli, $uid);
        $newPw = mysqli_real_escape_string($this->mysqli, $newPw);

        if (!$stmt = $this->mysqli->prepare("UPDATE User SET Passwort=? WHERE UserId=?;")) {
            echo "Prepare failed: (" . $this->mysqli->errno . ") " . $this->mysqli->error;
            return false;
        }

        if (!$stmt->bind_param('si', $newPw, $uid)) {
            echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
            return false;
        }
        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            return false;
        }

        if ($stmt->affected_rows != 1) {
            return false;
        }

        return true;
    }

    /*
     * Gibt User Array zurueck mit vorname und nachname. [0] -> vorname, [1] -> nachname
     */
    public function getUserDetail($id) {
        $id = mysqli_real_escape_string($this->mysqli, $id);

        if (!($stmt = $this->mysqli->prepare('SELECT Vorname, Nachname FROM User WHERE UserId=?'))) {
            echo "Prepare failed: (" . $this->mysqli->errno . ") " . $this->mysqli->error;
        }

        if (!$stmt->bind_param("i", $id)) {
            echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
        }

        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }

        $stmt->bind_result($vorname, $nachname);
        $stmt->fetch();

        return $array = array($vorname, $nachname);
    }


    public function deleteUser($userid) {

    }

    public function addUserRechte($user, $anlage, $recht) {
        $user = mysqli_real_escape_string($this->mysqli, $user);
        $anlage = mysqli_real_escape_string($this->mysqli, $anlage);
        $recht = mysqli_real_escape_string($this->mysqli, $recht);

        if (!$stmt = $this->mysqli->prepare("INSERT INTO UserAnlagenRechte(User, Anlage, Rechte) VALUES(?, ?, ?)")) {
            echo "Prepare failed: (" . $this->mysqli->errno . ") " . $this->mysqli->error;
            return false;
        }

        if (!$stmt->bind_param('iii', $user, $anlage, $recht)) {
            echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
            return false;
        }
        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            return false;
        }

        if ($stmt->affected_rows != 1) {
            return false;
        }

        return true;
    }

    public function addAnlageAndUserAsAdmin($name, $beschreibung, $ort, $plz, $strasse, $uid) {
        $lastid = $this->insertAnlage($name, $beschreibung, $ort, $plz, $strasse);

        if($lastid == 0) {
            return false;
        }

        if ($this->addUserRechte($uid, $lastid, '1')) {
            return true;
        } else {
            return false;
        }

    }

    public function removeUserRechteVonAnlage($user, $anlage) {

    }

    public function printOwnAnlagenDropdown($uid) {
        $uid = mysqli_real_escape_string($this->mysqli, $uid);

        if (!($stmt = $this->mysqli->prepare('SELECT AnlageId, Name FROM Anlage, UserAnlagenRechte WHERE AnlageId = Anlage AND User=? AND Rechte=1'))) {
            echo "Prepare failed: (" . $this->mysqli->errno . ") " . $this->mysqli->error;
        }

        if (!$stmt->bind_param('i', $uid)) {
            echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
            return false;
        }

        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }

        $stmt->bind_result($id, $name);

        echo '<select name="anlagen_id" id="anlagen_liste">';
        while ($stmt->fetch()) {
            printf ('<option value="%d">%s</option>', $id, $name);
        }
        echo '</select>';
    }



    // ----------------------------------------- Kommentar Funktionen -----------------------------------------

    /*
     * Gibt mehrdimensionales Array zurueck, welches ein Array mit [0] = Autorname, [1] = Kommentar und [2] = Zeit (z.B.: 2013-09-19 10:45:37) beinhaltet.
     */
    public function getKommentareFuerBallweg($anlage, $bahn, $ballweg) {
        $anlage = mysqli_real_escape_string($this->mysqli, $anlage);
        $bahn = mysqli_real_escape_string($this->mysqli, $bahn);
        $ballweg = mysqli_real_escape_string($this->mysqli, $ballweg);

        if (!($stmt = $this->mysqli->prepare('SELECT Username, Kommentar, Zeit, Kommentar.Autor, KommentarNummer FROM Kommentar, User WHERE Kommentar.Autor = User.UserId AND Anlage=? AND Bahn=? AND Ballweg=?'))) {
            echo "Prepare failed: (" . $this->mysqli->errno . ") " . $this->mysqli->error;
        }

        if (!$stmt->bind_param("iii", $anlage, $bahn, $ballweg)) {
            echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
        }

        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }

        $stmt->store_result();

        if ($stmt->num_rows == 0) { //TODO ggf. fehler
            return;
        }
        $zeit=''; $uid=''; $kommId='';
        $stmt->bind_result($username, $kommentar, $zeit, $uid, $kommId);

        $kommentare = array();

        while($stmt->fetch()) {
            $kommentar = array($username, $kommentar, $zeit, $uid, $kommId);
            $kommentare[] = $kommentar;
        }

        return $kommentare;
    }

    /*
     * Speichert einen Kommentar in der Datenbank. $Autor ist die ID des Users, nicht sein Name!
     */
    public function insertKommentar($anlage, $bahn, $weg, $kommentar, $autor) {
        $anlage = mysqli_real_escape_string($this->mysqli, $anlage);
        $bahn = mysqli_real_escape_string($this->mysqli, $bahn);
        $weg = mysqli_real_escape_string($this->mysqli, $weg);
        $autor = mysqli_real_escape_string($this->mysqli, $autor);
        $kommentar = mysqli_real_escape_string($this->mysqli, $kommentar);

        if (!$stmt = $this->mysqli->prepare("INSERT INTO Kommentar(Anlage, Bahn, Ballweg, Kommentar, Autor) VALUES(?, ?, ?, ?, ?)")) {
            echo "Prepare failed: (" . $this->mysqli->errno . ") " . $this->mysqli->error;
            return false;
        }

        if (!$stmt->bind_param('iiisi', $anlage, $bahn, $weg, $kommentar, $autor)) {
            echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
            return false;
        }
        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            return false;
        }

        if ($stmt->affected_rows != 1) {
            return false;
        }

        return true;
    }

    public function deleteKommentar($anlage, $bahn, $weg, $kommentarNummer) {
        $anlage = mysqli_real_escape_string($this->mysqli, $anlage);
        $bahn = mysqli_real_escape_string($this->mysqli, $bahn);
        $weg = mysqli_real_escape_string($this->mysqli, $weg);
        $kommentarNummer = mysqli_real_escape_string($this->mysqli, $kommentarNummer);


        if (!$stmt = $this->mysqli->prepare("DELETE FROM Kommentar WHERE Anlage=? AND Bahn=? AND Ballweg=? AND KommentarNummer=?")) {
            echo "Prepare failed: (" . $this->mysqli->errno . ") " . $this->mysqli->error;
            return false;
        }

        if (!$stmt->bind_param('iiii', $anlage, $bahn, $weg, $kommentarNummer)) {
            echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
            return false;
        }
        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            return false;
        }

        if ($stmt->affected_rows != 1) {
            return false;
        }

        return true;
    }

    public function hasModRights($anlage, $uid) {
        $anlage = mysqli_real_escape_string($this->mysqli, $anlage);
        $uid = mysqli_real_escape_string($this->mysqli, $uid);


        if (!$stmt = $this->mysqli->prepare("SELECT Rechte FROM UserAnlagenRechte WHERE Anlage=? AND User=? AND (Rechte=1 OR Rechte=2)")) {
            echo "Prepare failed: (" . $this->mysqli->errno . ") " . $this->mysqli->error;
            return false;
        }

        if (!$stmt->bind_param('ii', $anlage, $uid)) {
            echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
            return false;
        }
        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            return false;
        }

        $stmt->store_result();

        if ($stmt->num_rows == 0) {
            return false;
        }

        return true;
    }

    public function isOwnKommentar() {

    }

    public function updateKommentar($anlage, $bahn, $weg, $kommentarNummer, $kommentar) {
        $anlage = mysqli_real_escape_string($this->mysqli, $anlage);
        $bahn = mysqli_real_escape_string($this->mysqli, $bahn);
        $weg = mysqli_real_escape_string($this->mysqli, $weg);
        $kommentarNummer = mysqli_real_escape_string($this->mysqli, $kommentarNummer);
        $kommentar = mysqli_real_escape_string($this->mysqli, $kommentar);

        if (!$stmt = $this->mysqli->prepare("UPDATE User SET Kommentar=? WHERE Anlage=? AND Bahn=? AND Ballweg=? AND KommentarNummer=?")) {
            echo "Prepare failed: (" . $this->mysqli->errno . ") " . $this->mysqli->error;
            return false;
        }

        if (!$stmt->bind_param('siiii', $kommentar, $anlage, $bahn, $weg, $kommentarNummer)) {
            echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
            return false;
        }
        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            return false;
        }

        if ($stmt->affected_rows != 1) {
            return false;
        }

        return true;
    }

    // ----------------------------------------- Linienzeichen Funktionen -----------------------------------------

    /*
    * Verarbeitet Mehrdimensionales Array zu einzelnen Linien eines Ballweges.
    */
    public function insertBallweglinien($array, $anlageid, $bahnid, $wegid) {
        if (!$stmt = $this->mysqli->prepare("INSERT INTO Ballwegelinie(Anlage, Bahn, Ballweg, KoordX, KoordY) VALUES(?, ?, ?, ?, ?)")) {
            echo "Prepare failed: (" . $this->mysqli->errno . ") " . $this->mysqli->error;
            return false;
        }

        $weg=''; $x=''; $y='';
        if (!$stmt->bind_param('iiiii', $anlage, $bahn, $weg, $x, $y)) {
            echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
            return false;
        }

        foreach ($array as &$row) {
            $anlage = $anlageid;
            $bahn = $bahnid;
            $weg = $wegid;
            $x = $row[0];
            $y = $row[1];


            if (!$stmt->execute()) {
                printf ("%d, %d, %d, %d, %d", $anlage, $bahn, $weg, $x, $y);
                echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
                return false;
            }
        }

        return true;
    }

    /*
     * Gibt JavaScript Code aus.
     * - Zuerst wird draw_start(start_x, start_y) aufgerufen um Linienzeichnen vorzubereiten (Startpunkt setzen)
     * - Pro Eintrag in der Datenbank wird einmal draw(x, y) aufgerufen
     * - Am ende wird zum abschluss draw_start() aufgerufen
     */
    public function  getBallWegeKoords($anlage, $bahnNummer, $wegNummer) {
        $anlage = mysqli_real_escape_string($this->mysqli, $anlage);
        $bahnNummer = mysqli_real_escape_string($this->mysqli, $bahnNummer);
        $wegNummer = mysqli_real_escape_string($this->mysqli, $wegNummer);

        if (!($stmt = $this->mysqli->prepare('SELECT KoordX, KoordY FROM Ballwegelinie WHERE Anlage=? AND Bahn=? AND Ballweg=?'))) {
            echo "Prepare failed: (" . $this->mysqli->errno . ") " . $this->mysqli->error;
        }

        if (!$stmt->bind_param("iii", $anlage, $bahnNummer, $wegNummer)) {
            echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
        }

        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }

        $stmt->store_result();

        if ($stmt->num_rows == 0) {
            $this->errorMessage('Fehler beim auslesen der Koordinaten! Anlage'.$anlage.' mit Bahn '.$bahnNummer.' und Weg '.$wegNummer.' nicht gefunden!');
            return;
        }

        $stmt->bind_result($koordX, $koordY);

        //echo '</br><script type="text/javascript">draw_start();';
        echo 'draw_start(60, 150);';//TODO: Startwerte aus DB! (?)
        while($stmt->fetch()) {
            printf ("\ndraw(dbKoordForCanvas(%d), dbKoordForCanvas(%d));", $koordX, $koordY);
        }
        printf ("\ndraw_end();");
        //printf ("\ndraw_end();</script></br>");
    }

    /*
     *  Gibt Bahntyp zurueck
     */
    public function getBahnTyp($anlage, $bahn) {
        $anlage = mysqli_real_escape_string($this->mysqli, $anlage);
        $bahn = mysqli_real_escape_string($this->mysqli, $bahn);

        //if (!($stmt = $this->mysqli->prepare('SELECT Anlage.Name, BahnNummer, Bahn.Name, FROM Bahn, Anlage WHERE Anlage.AnlageId=Bahn.Anlage AND Bahn.Anlage=? AND Bahn.BahnNummer=?'))) {
        if (!($stmt = $this->mysqli->prepare('SELECT BahnTyp FROM Bahn WHERE Anlage=? AND BahnNummer=?'))) {
            echo "Prepare failed: (" . $this->mysqli->errno . ") " . $this->mysqli->error;
        }

        if (!$stmt->bind_param("ii", $anlage, $bahn)) {
            echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
        }

        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }

        $stmt->bind_result($bahnTyp);

        $stmt->fetch();

        return $bahnTyp;
    }

    /*
    public function getFreeWegeId($anlage, $bahn) {
        $anlage = mysqli_real_escape_string($this->mysqli, $anlage);
        $bahn = mysqli_real_escape_string($this->mysqli, $bahn);

        if (!($stmt = $this->mysqli->prepare('SELECT WegNummer FROM Ballweg WHERE Anlage=? AND Bahn=? ORDER BY WegNummer DESC LIMIT 1'))) {
            echo "Prepare failed: (" . $this->mysqli->errno . ") " . $this->mysqli->error;
        }

        if (!$stmt->bind_param("ii", $anlage, $bahn)) {
            echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
        }

        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }

        $ballart='';
        $stmt->bind_result($newWegNummer);

        $stmt->fetch();

        return ($newWegNummer+1);
    }*/

    /*
     * Liest die in der DB eingetragenen Ballarten aus und gibt ein Dropdownmenue mit den Moeglichkeiten zurueck
     * (id=ballart_liste)
     */
    public function printBallArtenDropdown() {
        if (!($stmt = $this->mysqli->prepare('SELECT * FROM BallArt'))) {
            echo "Prepare failed: (" . $this->mysqli->errno . ") " . $this->mysqli->error;
        }

        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
        }

        $stmt->bind_result($id, $art);

        echo '<select id="ballart_liste">';
        while ($stmt->fetch()) {
            printf ('<option value="%d">%s</option>', $id, $art);
        }
        echo '</select>';
    }


    public function insertBahn($anlage, $bahn, $name, $bahntyp) {
        $anlage = mysqli_real_escape_string($this->mysqli, $anlage);
        $bahn = mysqli_real_escape_string($this->mysqli, $bahn);
        $name = mysqli_real_escape_string($this->mysqli, $name);
        $bahntyp = mysqli_real_escape_string($this->mysqli, $bahntyp);

        if (!$stmt = $this->mysqli->prepare("INSERT INTO Bahn(Anlage, BahnNummer, Name, BahnTyp) VALUES(?, ?, ?, ?)")) {
            echo "Prepare failed: (" . $this->mysqli->errno . ") " . $this->mysqli->error;
            return false;
        }

        if (!$stmt->bind_param('iisi', $anlage, $bahn, $name, $bahntyp)) {
            echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
            return false;
        }
        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            return false;
        }

        if ($stmt->affected_rows != 1) {
            return false;
        }

        return true;
    }

    public function insertBallweg($anlage, $bahn, $autor, $kommentar, $ballArt) {
        $anlage = mysqli_real_escape_string($this->mysqli, $anlage);
        $bahn = mysqli_real_escape_string($this->mysqli, $bahn);
        $autor = mysqli_real_escape_string($this->mysqli, $autor);
        $kommentar = mysqli_real_escape_string($this->mysqli, $kommentar);
        $ballArt = mysqli_real_escape_string($this->mysqli, $ballArt);

        if (!$stmt = $this->mysqli->prepare("INSERT INTO Ballweg(Anlage, Bahn, Name, Kommentar, BallArt) VALUES(?, ?, ?, ?, ?)")) {
            echo "Prepare failed: (" . $this->mysqli->errno . ") " . $this->mysqli->error;
            return 0;
        }

        if (!$stmt->bind_param('iiisi', $anlage, $bahn, $autor, $kommentar, $ballArt)) {
            echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
            return 0;
        }
        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            return 0;
        }

        if ($stmt->affected_rows != 1) {
            return 0;
        }

        return $this->mysqli->insert_id;
    }


    public function insertBallwegAndAddBallweglinien($array) {
        $infoarray = $array[0];
        $wegarray = $array[1];
        $koordsarray = $array[2];

        $lastid = $this->insertBallweg($infoarray[0], $infoarray[1], $wegarray[0], $wegarray[1], $wegarray[2]);

        if ($lastid == 0) {
            return false;
        }

        return ($this->insertBallweglinien($koordsarray, $infoarray[0], $infoarray[1], $lastid));
    }

    public function checkForAnlage($anlage) {
        $anlage = mysqli_real_escape_string($this->mysqli, $anlage);

        if (!($stmt = $this->mysqli->prepare('SELECT * FROM Anlage WHERE AnlageId=?'))) {
            echo "Prepare failed: (" . $this->mysqli->errno . ") " . $this->mysqli->error;
            return false;
        }


        if (!$stmt->bind_param("i", $anlage)) {
            echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
            return false;
        }

        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            return false;
        }

        $stmt->store_result();

        if ($stmt->num_rows != 0) {
            return true;
        }

        return false;
    }

    public function checkForBahn($anlage, $bahn) {
        $anlage = mysqli_real_escape_string($this->mysqli, $anlage);
        $bahn = mysqli_real_escape_string($this->mysqli, $bahn);

        if (!($stmt = $this->mysqli->prepare('SELECT * FROM Bahn WHERE Anlage=? AND BahnNummer=?'))) {
            echo "Prepare failed: (" . $this->mysqli->errno . ") " . $this->mysqli->error;
            return false;
        }


        if (!$stmt->bind_param("ii", $anlage, $bahn)) {
            echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
            return false;
        }

        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            return false;
        }

        $stmt->store_result();

        if ($stmt->num_rows != 0) {
            return true;
        }

        return false;
    }

    public function checkForWeg($anlage, $bahn, $weg) {
        $anlage = mysqli_real_escape_string($this->mysqli, $anlage);
        $bahn = mysqli_real_escape_string($this->mysqli, $bahn);
        $weg = mysqli_real_escape_string($this->mysqli, $weg);

        if (!($stmt = $this->mysqli->prepare('SELECT * FROM Ballweg WHERE Anlage=? AND Bahn=? AND WegNummer=?'))) {
            echo "Prepare failed: (" . $this->mysqli->errno . ") " . $this->mysqli->error;
            return false;
        }


        if (!$stmt->bind_param("iii", $anlage, $bahn, $weg)) {
            echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
            return false;
        }

        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            return false;
        }

        $stmt->store_result();

        if ($stmt->num_rows != 0) {
            return true;
        }

        return false;
    }

    public function errorMessage($msg) {
        echo '<script type="text/javascript">errorMessage("'.$msg.'");</script>';
    }

    public function anlageHatBahn($anlage) {
        $anlage = mysqli_real_escape_string($this->mysqli, $anlage);

        if (!($stmt = $this->mysqli->prepare('SELECT * FROM Bahn WHERE Anlage=?'))) {
            echo "Prepare failed: (" . $this->mysqli->errno . ") " . $this->mysqli->error;
            return false;
        }


        if (!$stmt->bind_param("i", $anlage)) {
            echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
            return false;
        }

        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            return false;
        }

        $stmt->store_result();

        if ($stmt->num_rows != 0) {
            return true;
        }

        return false;
    }

    public function bahnHatWege($anlage, $bahn) {
        $anlage = mysqli_real_escape_string($this->mysqli, $anlage);
        $bahn = mysqli_real_escape_string($this->mysqli, $bahn);

        if (!($stmt = $this->mysqli->prepare('SELECT * FROM Ballweg WHERE Anlage=? AND Bahn=?'))) {
            echo "Prepare failed: (" . $this->mysqli->errno . ") " . $this->mysqli->error;
            return false;
        }


        if (!$stmt->bind_param("ii", $anlage, $bahn)) {
            echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
            return false;
        }

        if (!$stmt->execute()) {
            echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            return false;
        }

        $stmt->store_result();

        if ($stmt->num_rows != 0) {
            return true;
        }

        return false;
    }
}

?>