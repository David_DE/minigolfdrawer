<?php
session_start();
ini_set('display_errors', 'On');
include("Minigolf.php");
$mini = new Minigolf();


//Registrieren Button
if (isset($_POST['submit_regist'])) {
	header("Location:register.php"); 
	exit();
}

//Register
if (isset($_POST['submit_register'])) {
	if ($_POST['username_register'] == "" OR $_POST['password_register'] == "" 
	OR $_POST['password2_register'] == "" OR $_POST['name_register'] == "" OR $_POST['vorname_register'] == "") {

		$_POST['error'] = 'Sie m&uuml;ssen alle Felder ausf&uuml;llen.';
		//header("Location:register.php"); 	
		//exit;
	
	} else if($_POST['password_register'] != $_POST['password2_register']){
		
		$_POST['error'] = 'Die Passw&ouml;rter stimmen nicht &uuml;berein.';
		//header("Location:register.php"); 
		//exit;
		
	} else {
		$username_register = $_POST["username_register"];
		$password_register = $_POST["password_register"];
		$password2_register = $_POST["password2_register"];
		$name_register = $_POST["name_register"];
		$vorname_register = $_POST["vorname_register"];
		
		$password_register = md5($password_register);
		
		$id_register = $mini->getUserId($username_register);
			
		//Username darf nur einmal vorkommen //wenn ungleich 0 gibts namen schon
		if($id_register == 0){ 
			if($mini->insertUser($username_register,$password_register,$vorname_register,$name_register)){
				//wenn erfolgreich in Datenbank eingetragen
				$_POST['error'] = 'Benutzername <b>'.$username.'</b> wurde erstellt.';
				$_SESSION['username']=$username_register;
				setcookie("username", $username_register, time()+30*24*3600);
				$_SESSION['ID']=$id;
				setcookie("ID", $id, time()+30*24*3600);
			} else {
				$_POST['error'] = 'Fehler beim Speichern des Benutzernames.';
			}       	
		} else {
			$_POST['error'] = 'Benutzername schon vorhanden.';
		}	
	}
		
}
	
//Login
if (isset($_POST['submit_login'])) {
	if ($_POST['username_login'] != "" && $_POST['password_login'] != "") {
		$username_login = $_POST["username_login"];
		$passwort_login = md5($_POST["password_login"]);
		
		if($mini->validateUserPw($username_login, $passwort_login)) {
			$_SESSION["username"] = $username_login;
			setcookie("username", $username_login, time()+30*24*3600);
		
			$id_login = $mini->getUserId($username_login);
			$_SESSION['ID']=$id_login;
			setcookie("ID", $id_login, time()+30*24*3600);
			header("Location: index.php" );
			exit();
		} else {
			$_POST['error'] = "Das Passwort ist nicht korrekt!";
			//header("Location: index.php");
			//exit();
		}
	} else {
		$_POST['error'] = "Sie m&uuml;ssen alle Felder ausf&uuml;llen.";
		//header("Location: index.php");
		//exit(); 
	}
}
//Logout
if ($_GET['type']=='logout') {
	setcookie("username", "", time()-3600);
	setcookie("ID", "", time()-3600);
    session_destroy();
	header("Location:index.php"); 
    exit;
}

//Kommentar
if (isset($_POST['submit_kommentar'])) {
	if (isset($_SESSION['ID'])) {
		$anlage = $_GET['anlage'];
		$bahn = $_GET['bahn'];
		$weg = $_GET['weg'];
		$autor_kommentar = $_SESSION['ID'];
		$kommentar = $_POST['kommentar'];
		
		if ($_POST['kommentar'] == "") {
			$_POST['error'] = 'Sie m&uuml;ssen alle Felder ausf&uuml;llen.';
		} else {
			$mini->insertKommentar($anlage, $bahn, $weg, $kommentar, $autor_kommentar);
			header('Location: index.php?anlage='.$_GET['anlage']. '&bahn=' .$_GET['bahn'] . '&weg=' .$_GET['weg']);
			exit();
		}
	} else {
		$_POST['error'] = 'Sie m&uuml;ssen eingeloggt sein, um Kommentare zu posten.';		
	}
}

//Ausgabe Kommentar
function printKommi() {
	$mini = new Minigolf();
	$anlage = $_GET['anlage'];
	$bahn = $_GET['bahn'];
	$ballweg = $_GET['weg'];

	$get_comment = $mini->getKommentareFuerBallweg($anlage, $bahn, $ballweg);
	if (count($get_comment) != 0) {
		
		if (isset($_SESSION['ID'])){
			$uid = $_SESSION['ID'];
			if ($mini->hasModRights($anlage, $uid)) {
				$icon = '<form method="post"><button type="submit" name="delete" class="delete_icon"><img src="img/icon_delete.png"></button></form>';
			} else {
				$icon ="";
			}
		} else {
			$icon ="";
		}
		
		echo '<table>';
		foreach ($get_comment as $comment) {
			echo
			'<tr>
			<td valign="top">Von <b>' . $comment[0] . '</b> am ' . $comment[2] . '</td><td></td>
			</tr>
			<tr>
			<td>' .	$comment[1] .'<td>
			<td>' . $icon.'</td>
			</tr>
			<tr>
			<td colspan="2">
			<hr width="400px">
			</td>
			</tr>
			';
		}
		echo '</table>';
	} else {
		echo 'Es sind noch keine Kommentare zu diesem Ballweg vorhanden.';
	}

//Kommentar löschen
	if (isset ($_POST['delete'])) {	
		$mini->deleteKommentar($anlage, $bahn, $ballweg, $comment[4]);
		header('Location: index.php?anlage='.$_GET['anlage']. '&bahn=' .$_GET['bahn'] . '&weg=' .$_GET['weg']);
		exit();
	}
}

//Kontaktformular
 if (isset($_POST["submit_contact"])) {
	if ($_POST['name_contact'] == "" OR $_POST['name_contact'] == "" OR $_POST['email_contact' ] == "" OR $_POST['betreff_contact'] == "" OR $_POST['betreff_contact'] == "") {
		$_POST['error'] = 'Bitte alle Felder ausf&uuml;llen!';
	} else {
		// Formulardaten
		$an = "christian@4schaefer-home.de";
		$name_contact = $_POST['name_contact'];
		$email_contact = $_POST['email_contact'];
		$betreff_contact = $_POST['betreff_contact'];
		$nachricht_contact = $_POST['betreff_contact'];
		
		// Nachrichtenlayout erstellen
		$message = "
		Name:           $name_contact
		Email:          $email_contact
		Nachricht:      $nachricht_contact
		";
		
		// Verschicken der Mail
		mail($an, $betreff_contact, $message);
		$_POST['error'] = 'Nachricht wurde versand.';
	}
}


//Profil bearbeiten
//Namen ändern
if (isset($_POST['submit_name_aendern'])) {
	if ($_POST['vorname_aendern'] =="" OR $_POST['nachname_aendern'] =="") {
 		$_POST['error'] = 'Bitte alle Felder ausf&uuml;llen!';
	} else {
		$vorname_neu = $_POST['vorname_aendern'];
		$nachname_neu = $_POST['nachname_aendern'];
		$id_profil_bearbeiten = $_SESSION['ID'];
		if ($mini->updateUserDetails($id_profil_bearbeiten, $vorname_neu, $nachname_neu)) {
			$_POST['error'] = 'Namen erfolgreich geändert.';
		} else {
			$_POST['error'] = 'Name konnte nicht geändert werden.';
		}
	}
}
//Passwort ändern 
if (isset($_POST['submit_pw_aendern'])) {
	$aktuelles_pw = $_POST['aktuelles_pw'];
	$neues_pw1 = $_POST['neues_pw1'];
	$neues_pw2 = $_POST['neues_pw2'];

	if ($aktuelles_pw =="" OR $neues_pw1 =="" OR $neues_pw2 =="") {
		$_POST['error'] = 'Bitte alle Felder ausf&uuml;llen!';
	} else {
		$username_pw_aendern = $_SESSION["username"];
		$passwort_aktuell = md5($_POST["aktuelles_pw"]);
		$id_pw_aendern = $_SESSION['ID'];

		if($mini->validateUserPw($username_pw_aendern, $passwort_aktuell)) {
			if ($neues_pw1 == $neues_pw2) {
				$neues_pw1 = md5($neues_pw1);
				if($mini->updateUserPassword($id_pw_aendern, $neues_pw1)){
					$_POST['error'] = 'Passwort wurde erfolgreich ge&auml;ndert.';
				} else {
					$_POST['error'] = 'Passwort wurde nicht ge&auml;ndert.';
				}
			} else {
				$_POST['error'] = 'Passw&ouml;rter stimmen nicht &uuml;berein.';
			}
		} else {
			$_POST['error'] = 'Aktuelles Passwort ist nicht korrekt.';			
		}
	}
}

//Anlage hinzufügen
if (isset($_POST['submit_anlage'])) {
	$name_anlage = $_POST['name_anlage'];
	$beschreibung = $_POST['beschreibung'];
	$strasse = $_POST['strasse'];
	$plz = $_POST['plz'];
	$ort = $_POST['ort'];
	$user_id_anlage = $_SESSION['ID'];
	
	if ($name_anlage=="" OR $beschreibung=="" OR $strasse=="" OR $plz=="" OR $ort=="") {
		$_POST['error'] = 'Bitte alle Felder ausf&uuml;llen!';
	} else {
		if ($mini->addAnlageAndUserAsAdmin($name_anlage, $beschreibung, $ort, $plz, $strasse, $user_id_anlage)) {
			$_POST['error'] = 'Anlage erfolgreich hinzugef&uuml;gt.';
		} else {
			$_POST['error'] = 'Anlage konnte nicht hinzugef&uuml;gt werden.';
		}
	}
}

//Bahn hinzufügen
if (isset($_POST['submit_bahn'])) {
	$anlagen_id = $_POST['anlagen_id'];
	$name_bahn = $_POST['name_bahn'];
	$nummer_bahn = $_POST['nummer_bahn'];
	$bahntyp = $_POST['bahntyp'];
	
	if ($name_bahn=="" OR $nummer_bahn=="" OR !isset($bahntyp) OR !isset($anlagen_id)) {
		$_POST['error'] = 'Bitte alle Felder ausf&uuml;llen!';
	} else {
		if ($mini->insertBahn($anlagen_id, $nummer_bahn, $name_bahn, $bahntyp)) {
			$_POST['error'] = 'Bahn erfolgreich hinzugef&uuml;gt.';
		} else {
			$_POST['error'] = 'Bahn konnte nicht hinzugef&uuml;gt werden.';
		}
	}
}
?>