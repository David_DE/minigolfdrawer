<?php
ob_start();
header('Content-Type: text/html;charset=utf-8');
include("php/functions.php");
?>
<!DOCTYPE html>
<html>
<head>
    <title>Minigolf-Drawer</title>

    <!--<meta name="viewport" content="width=device-width, initial-scale=1.0">-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
	<link href="css/design.css" rel="stylesheet">
    <script language="javascript" type="text/javascript" src="js/main.js"></script>
    <script language="javascript" type="text/javascript" src="js/draw.js"></script>
    <script language="javascript" type="text/javascript" src="js/canvas.js"></script>

</head>
<body>
    <?php
    error_reporting(E_ALL);
    function __autoload($class_name) {
        include 'php/' . $class_name . '.php';
    }

    $mini = new Minigolf();
	
    ?>
    <div class="navbar">
        <div class="navbar-inner">
            <div class="container">

                <!-- .btn-navbar is used as the toggle for collapsed navbar content -->
                <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>

                <!-- Be sure to leave the brand out there if you want it shown -->
                <a class="brand" href="index.php">Minigolf-Drawer</a>

                <!-- Everything you want hidden at 940px or less, place within here -->
                <div class="nav-collapse collapse">
                    <ul class="nav">
                    	<li><a href="index.php">Home</a></li>
                        <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Anlagen<b class="caret"></b></a>
                        	<ul id="navi_anlagen_dropdown" class="dropdown-menu">
                        		<?php
                            		$mini->getAlleAnlagenFuerNavi();
                        		?>
                        	</ul>
                        </li>
                     	<?php
                        	if (isset($_GET["anlage"])) {
                            	echo '<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">Bahnen<b class="caret"></b></a>';
                            	echo '<ul id="navi_bahnen_dropdown" class="dropdown-menu">';
                            	$mini->getAlleBahnenFuerNavi($_GET["anlage"]);
                            	echo '</ul>';
                             }
                        ?>
                        </li>
                       <?php
                            if (isset($_GET["anlage"]) && isset($_GET["bahn"])) {
                            	echo '<li class="dropdown" class="active">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">Ballwege<b class="caret"></b></a>';
                                echo '<ul id="navi_bahnen_dropdown" class="dropdown-menu">';
                                $mini->getAlleBallwegeFuerNavi($_GET["anlage"], $_GET["bahn"]); //$_GET["bahn"]
                                echo '</ul>';
                             }
                        ?>
                        <li><a href="help.php">Hilfe</a></li>
                        <li><a href="contact.php">Kontakt</a></li>
                    </ul>
                    <?php 
					if (isset($_SESSION['username']) && ($_SESSION['ID']) ) {
		    			if (isset($_COOKIE['username']) && isset($_COOKIE['ID'])) {
                			$_SESSION['username']=$_COOKIE['username'];
							$_SESSION['ID']=$_COOKIE['ID'];
							
			           		echo '<div class="navbar-form pull-right">Herzlich Willkommen <a href="profil.php">'
							.$_SESSION['username'].'</a> ! <a href="?type=logout">Logout</a></div>';
						
            			} else {
                			setcookie("username", "", time()-3600);
							setcookie("ID", "", time()-3600);
            		
							echo '
							<form class="navbar-form pull-right" method="post" action="">
							<input name="username_login" class="span2" type="text" placeholder="Username">
							<input name="password_login" class="span2" type="password" placeholder="Password">
							<button name="submit_login" class="btn" type="submit">Login</button>
							<button name="submit_regist" class="btn" type="submit">Registrieren</button>		
							</form>';
							
						}
					} else {
						echo '
							<form class="navbar-form pull-right" method="post" action="">
							<input name="username_login" class="span2" type="text" placeholder="Username">
							<input name="password_login" class="span2" type="password" placeholder="Password">
							<button name="submit_login" class="btn" type="submit">Login</button>
							<button name="submit_regist" class="btn" type="submit">Registrieren</button>		
							</form>';	
					}
					?> 	
                </div>
            </div>
        </div>
    </div>
    <div class="container">

        <div id="error">
            <?php
            if (isset($_POST["error"])) {
                echo $_POST["error"];
            }

            ?>
        </div>

        <div id="content">
            <h3>Registrieren</h3>
            <form method="post">
            Benutzername:<br>
            <input type="text" class="span2" placeholder="Username" name="username_register"><br>
            
            Vorname:<br>
            <input type="text" class="span2" placeholder="Vorname" name="vorname_register"><br>
            
            Name:<br>
            <input type="text" class="span2" placeholder="Name" name="name_register"><br>
            
            Passwort:<br>
            <input type="password" class="span2" placeholder="Password" name="password_register"><br>
            
            Passwort wiederholen:<br>
            <input type="password" class="span2" placeholder="Repeat Password" name="password2_register"><br><br>
            
             <input id="submit" class="btn" name="submit_register" type="submit" value="Registrieren" />
            </form>
        
            </div>
            <div id="footer">
   				&copy; David Frost Christian Sch&auml;fer Laura Unger
        	</div>
        </div>
    <!-- /container -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="http://code.jquery.com/jquery.js"></script>
    <script src="js/bootstrap.js"></script>
	
</body>
</html>
