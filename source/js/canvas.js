/*
 * Passt die Groeße des Canvas elements an Aufloesung an
 */
function fitCanvasToResulution() {
    var canvas = document.getElementById('canvas');
    var context = canvas.getContext('2d');

    //Standard Canvas breite -> 1150px bei 1280
    var scale = calcScale();
    canvas.width = 1150 * scale;
    canvas.height = 300 * scale;

    //console.log("W: " + canvas.width + " / h: " + canvas.height + " / scale: " + scale);
}



/*
 * Normalisiert Koordinaten, um sie fuer die standard canvas groesse passend zu machen (800)
 */
function normKoordForDB(koord) {
    koord /= calcScale();

    return koord;
}



/*
 * Passt die Koordinaten an, um relativ zur groesse des Canvases die Richtige Position zu haben
 */
function dbKoordForCanvas(koord) {
    koord *= calcScale();

    return koord;
}


/*
 * Berechnet Scale: Scale sagt aus wie gross/wo etwas gezeichnet werden muss,
 * um relativ zur Aufloesung zu sein.
 */
function calcScale() {
	var content_box = document.getElementById('content');
	var computedStyle = window.getComputedStyle(content_box, null);
	//alert ($("#content").width());

    var scale = $("#content").width() / 1200;

    if (scale > 1.25)
        scale = 1.25;

    return scale;
}

/*
 * Setzt den Click listener
 */
function setCanvasClick() {
    $('canvas').on('click', function(e) {
        var offset = $(this).offset();
        var x = e.pageX - offset.left;
        var y = e.pageY - offset.top;
        var koords = new Array(Math.round(normKoordForDB(x)), Math.round(normKoordForDB(y)));
        koords_array.push(koords);

        drawLinesWhileDrawing();
    });
}

function saveCanvas(anlage, bahn, userid) {
    var info_array = new Array(anlage, bahn);
    var gesammt_array = new Array(info_array);


    var ballart = $("#ballart_liste").val();
    var kommentar = $("#weg_kommentar").val();

    if (userid == 0) {
        errorMessage('Sie müssen eingeloggt sein!');
        return false;
    }

    if (koords_array.length == 0) {
        errorMessage('Bitte Ballweg einzeichnen!');
        return false;
    }

    if (kommentar.length == 0) {
        errorMessage('Bitte Kommentar eingaben!');
        return false;
    }

    var wege_array = new Array(userid, kommentar, ballart);

    gesammt_array.push(wege_array);
    gesammt_array.push(koords_array);

    postLinesToDb(gesammt_array);

    return true;
}

function resetCanvas(bahn) {
    koords_array = new Array();
    drawMinigolfBahn(bahn);
}