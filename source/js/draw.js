/*
 * draw_start() Soll vor draw() und draw_end() ausgeführt werden und initialisiert das Canvas Element
 */
function draw_start(start_x, start_y) {
    var canvas = document.getElementById('canvas');
    var context = canvas.getContext('2d');

    //context.fillStyle="#088A29";
    //context.fillRect(0, 0, canvas.width, canvas.height);

    context.beginPath();
    context.lineWidth = 3;
    context.strokeStyle = '#000000';
    //context.moveTo(dbKoordForCanvas(75),dbKoordForCanvas(150));
    context.moveTo(dbKoordForCanvas(start_x),dbKoordForCanvas(start_y));
}



/*
 * draw_end() beendet das Zeichnen der Linien in die Canvas
 */
function draw_end() {
    var canvas = document.getElementById('canvas');
    var context = canvas.getContext('2d');

    context.stroke();
    context.closePath();
}



/*
 * draw() zeichnet eine Linie vom letzten uebergebenen Punkt zum aktuell uebergebenen.
 * In draw_start() wird der Startpunkt festgelegt
 */
function draw(x, y) {
    var canvas = document.getElementById('canvas');
    var context = canvas.getContext('2d');

    //console.log("x: " + x + " / y: " + y);
    context.lineTo(x, y);
}

function drawLinesWhileDrawing() {
    //drawMinigolfBahn();
    //console.log("in drawLinesWhileDrawing()");
    draw_start(60, 150);

    for (var i = 0; i < koords_array.length; i++) {
        var k = koords_array[i];
        draw(dbKoordForCanvas(k[0]), dbKoordForCanvas(k[1]));
    }

    draw_end();
}

/*
 * Zeichnet MinigolfBahnen
 */
function drawMinigolfBahn(bahn){ //TODO: Parameter, der bestimmt welche Bahn gezeichnet wird
    var canvas = document.getElementById('canvas');
    var context = canvas.getContext('2d');

    var scale = calcScale();

    var CANVAS_WIDTH = 1150;
    var CANVAS_HEIGHT = 300;

    context.clearRect (0, 0, CANVAS_WIDTH*scale, CANVAS_HEIGHT*scale);

    /* Verwendete Farben:
     Grün Außen: #088A29
     Grün Bahn:  #04B404
     Weiß:       #FFFFFF
     Grau:       #A4A4A4
     Braun:      #6E5205
     */


    /* Standard Bahn */

    canvas.width = CANVAS_WIDTH*scale;
    canvas.height = CANVAS_HEIGHT*scale;

    context.fillStyle="#088A29";
    context.fillRect(0, 0, canvas.width, canvas.height);


    //Zielkreis
    context.beginPath();
    context.lineWidth = 2;
    context.strokeStyle = '#04B404';
    context.arc(1000*scale,150*scale,125*scale,0,Math.PI*2,false);
    context.fillStyle = '#04B404';
    context.fill();
    context.stroke();
    context.closePath();

    //Grenze Zielkreis
    context.beginPath();
    context.lineWidth = 1;
    context.strokeStyle = "#000000";
    context.arc(1000*scale,150*scale,120*scale,0,Math.PI*2,true);
    context.stroke();
    context.closePath();

    //Bahn gerade
    context.fillStyle = '#04B404';
    context.fillRect(20*scale, 88*scale, 874*scale, 125*scale);

    //Grenze Bahn
    context.beginPath();
    context.lineWidth = 1;
    context.strokeStyle = "#000000";
    context.moveTo(110*scale,94*scale);
    context.lineTo(895*scale,94*scale);
    context.moveTo(110*scale,207*scale);
    context.lineTo(895*scale,207*scale);
    context.stroke();
    context.closePath();

    //Abschlagsfeld

    context.fillStyle="#FFFFFF";
    context.fillRect(20*scale,88*scale,90*scale,125*scale);


    //Loch
    context.beginPath();
    context.lineWidth = 2;
    context.strokeStyle = "#FFFFFF";
    context.arc(1000*scale,150*scale,5*scale,0,Math.PI*2,true);
    context.fillStyle = "#A4A4A4";
    context.fill();
    context.stroke();
    context.closePath();


    //Abschlagspunkt
    context.beginPath();
    context.lineWidth = 2;
    context.strokeStyle = "#A4A4A4";
    context.arc(60*scale,150*scale,15*scale,0,Math.PI*2,true);
    context.stroke();
    context.closePath();
    context.beginPath();
    context.lineWidth = 2;
    context.strokeStyle = "#A4A4A4";
    context.arc(60*scale,150*scale,5*scale,0,Math.PI*2,true);
    context.stroke();
    context.closePath();

    //Hindernisse
    
    if(bahn==1)
    {
        // Bahn 1
        context.fillStyle="#6E5205";
        context.fillRect(950*scale, 100*scale, 30*scale, 30*scale);
        context.fillRect(1025*scale, 100*scale, 30*scale, 30*scale);
        context.fillRect(950*scale, 170*scale, 30*scale, 30*scale);
        context.fillRect(1025*scale, 170*scale, 30*scale, 30*scale);
    }
    else if(bahn==2)
    {
        //Bahn 2
        context.fillStyle="#6E5205";
        context.fillRect(500*scale, 88*scale, 150*scale, 55*scale);
        context.fillRect(500*scale, 158*scale, 150*scale, 55*scale);
    }
    else if(bahn==3)
    {
        //Bahn 3
        context.fillStyle="#6E5205";
        context.fillRect(300*scale, 88*scale, 50*scale, 70*scale);
        context.fillRect(500*scale, 143*scale, 50*scale, 70*scale);
        context.fillRect(700*scale, 88*scale, 50*scale, 70*scale);
    }
}