MinigolfDrawer
==============

Minigolf Drawer ist eine Webseite, auf der Minigolfanlagen von den Inhabern inklusive ihrer Bahnen eingetragen werden können. Zu jeder Bahn können User optimale Ballwege einzeichnen und abspeichern.

Die Webseite enstand im Verlauf der Veranstaltung Medienprojekt-Management (Medieninformatik) an der THM Friedberg und wurde erstellt von: 

Christian Schäfer  
David Frost  
Laura Unger  

#Dokumentation

##1. Einleitung

###1.1 Ziel und Zweck
Hauptziel der Webseite ist es Minigolf-Anlagen mit ihren Bahnen darzustellen. Der Schwerpunkt liegt dabei auf der Darstellung der einzelnen Bahnen. Dort sollen mittels HTML5 Canvas optimale Ballwege vorgeschlagen werden und auch eigene eingezeichnet werden können. Im Hinblick auf den praktischen Einsatz sollten auch mobile Endgeräte wie Smartphones und Tablets berücksichtigt werden. 

###1.2 Die Webseite
Die Webseite ist für Minigolfspieler. Auf der einen Seite für Anfänger, die sich nach optimalen Ballwegen für verschiedene Bahnen erkundigen können und auf der anderen Seite für Profis, die Ballwege einzeichnen können um Anderen damit zu helfen. Zu jedem Ballweg können auch Kommentare verfasst werden. Ballwege anschauen kann jeder, Ballwege einzeichnen oder Kommentare verfassen können nur registrierte Benutzer. 

##2. Verwendete Bibliotheken
###2.1 jQuery
jQuery ist eine freie JavaScript Bibliothek, die Funktionen zur DOM Manipulation zur Verfügung stellt. Dies erleichtert unter anderem auch das Verwenden von AJAX.

Webseite: [www.jquery.com](http://www.jquery.com/ "www.jquery.com")

###2.2 Bootstrap
Da diese Seite auch für mobile Endgeräte zur Verfügung gestellt werden soll, haben wir Bootstrap verwendet um das Layout auf eine einfache Art und Weise anzupassen und es auf jedem Endgerät entsprechend anzeigen zu lassen. 

Webseite: [getbootstrap.com](http://getbootstrap.com/ "getbootstrap.com")


##3. Installation und Inbetriebnahme
###3.1 Datenbank
Die Datenbankanbindung wird komplett von der PHP Klasse Minigolf übernommen. Um auf eine Datenbank zuzugreifen müssen die MySQL Benutzerdaten im Konstruktor der Klasse (Zeile 7) eingetragen werden:


$this->mysqli = new mysqli("localhost", "BENUTZER", "PASSWORT", "DB_NAME");

Die zu erstellende Datenbank muss aus folgenden Tabellen bestehen:  
* **Anlage:** Hier werden allgemeine Informationen zu den einzelnen Anlagen gespeichert  
* **Bahn:** Jeder Datensatz steht für eine Bahn einer Anlage. Aktuell bestehen die zusätzlichen Informationen nur aus dem Namen der Bahn.  
* **Ballweg:** Jeder Ballweg wird einer Anlage und Bahn zugeordnet, erhält die ID des Erstellers, ein Kommentar des Erstellers und die Ballart.  
* **Ballwegelinie:** Um sowohl einfache als auch komplexere Bahnen zu unterstützen kann jeder Ballweg beliebig viele Linien beinhalten. Jede Linie beinhaltet die Koordinate des Endpunktes. Der Anfangspunkt wird durch die vorherige Koordinate oder dem Startpunkt bestimmt.  
* **Ballart:** Beinhaltet nur den Namen des Balles um bei jedem Ballweg festzuhalten, welcher Ball benötigt wird.  
* **Kommentar:** Kommentare werden über die IDs mit Ballwegen verbunden, gleichzeitig beinhalten sie die User ID des Erstellers, die Zeit, sowie den eigentlichen Kommentartext.  
* **User:** Die Accounts der Benutzer werden hier mit Username, Passwort (nur MD5, keine Verschlüsselung), Vorname und Nachname gespeichert.  
* **Recht:** Hier befinden sich die Art der Rechte, die ein Benutzer bei einer bestimmten Anlage hat. (Administrator oder Mod)  
* **UserAnlagenRechte:** Eine Tabelle um bestimmten Usern spezielle Rechte bei verschiedenen Anlagen einzuräumen.   

**Hinweis:** Für den vollen SQL Code, siehe mini.sql. 

###3.2 Webseite einrichten
Um die Webseite einzurichten, müssen einfach der Ordner Minigolf auf den entsprechenden PHP-fähigen Server kopiert werden. 
###3.3 Wichtigste Funktionen
Die PHP Klasse Minigolf beinhaltet alle Funktionen zum Zugreifen auf die Datenbank. Diese baut in ihrem Konstruktor die eigentliche MySQL Verbindung auf und ermöglicht dadurch den einzelnen Funktionen Zugriff auf diese.

Die Funktionen decken die Bedürfnisse der Webseite ab, bestimmte Daten auszulesen, einzutragen oder zu ändern.
Dazu gehören beispielsweise: insertAnlage(), insertUser(), getUserId(), validatePassword(), etc.

Die SQL Statements werden jeweils über “Prepaired Statements” geregelt. Dort werden die Variablen nicht direkt mit dem Statement übergeben, sondern später gebunden und danach ausgeführt. So können auch größere Mengen an identischen Datensätzen hintereinander in die Datenbank geschrieben werden.


###3.4 Frontend (HTML/CSS)
Bei der Umsetzung des Frontends wurden sowohl HTML als auch CSS eingesetzt. Durch das Benutzen von Bootstrap ist das Design der Navigationsleiste nur eingeschränkt konfigurierbar gewesen. Insgesamt wurde weniger Wert auf das Design gelegt, sondern mehr darauf geachtet, dass die Funktionalität der Webseite sowohl am Desktop-PC als auch am Smartphone und am Tablet gewährleistet ist.
