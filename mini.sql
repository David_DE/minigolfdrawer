﻿-- --------------------------------------------------------
-- Host:                         frost.serverbros.co.uk
-- Server version:               5.5.32-cll - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL version:             7.0.0.4053
-- Date/time:                    2013-09-22 16:57:57
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;

-- Dumping database structure for db_mini
CREATE DATABASE IF NOT EXISTS `db_mini` /*!40100 DEFAULT CHARACTER SET latin1 COLLATE latin1_german2_ci */;
USE `db_mini`;


-- Dumping structure for table db_mini.Anlage
CREATE TABLE IF NOT EXISTS `Anlage` (
  `AnlageId` int(11) NOT NULL AUTO_INCREMENT,
  `Beschreibung` varchar(250) CHARACTER SET utf8 NOT NULL,
  `Name` varchar(150) CHARACTER SET utf8 NOT NULL,
  `Ort` varchar(100) CHARACTER SET utf8 NOT NULL,
  `Plz` varchar(10) CHARACTER SET utf8 NOT NULL,
  `Strasse` varchar(150) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`AnlageId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;



-- Dumping structure for table db_mini.Bahn
CREATE TABLE IF NOT EXISTS `Bahn` (
  `Anlage` int(11) NOT NULL,
  `BahnNummer` int(11) NOT NULL,
  `Name` varchar(45) CHARACTER SET latin1 COLLATE latin1_german2_ci NOT NULL,
  `BahnTyp` int(10) NOT NULL DEFAULT '1',
  PRIMARY KEY (`Anlage`,`BahnNummer`),
  KEY `fk_MinigolfBahn_Anlage1_idx` (`Anlage`),
  CONSTRAINT `fk_MinigolfBahn_Anlage1` FOREIGN KEY (`Anlage`) REFERENCES `Anlage` (`AnlageId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



-- Dumping structure for table db_mini.BallArt
CREATE TABLE IF NOT EXISTS `BallArt` (
  `BallId` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(45) CHARACTER SET latin1 COLLATE latin1_german2_ci NOT NULL,
  PRIMARY KEY (`BallId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table db_mini.BallArt: ~3 rows (approximately)
/*!40000 ALTER TABLE `BallArt` DISABLE KEYS */;
INSERT INTO `BallArt` (`BallId`, `Name`) VALUES
	(1, 'Hart'),
	(2, 'Weich'),
	(3, 'Flummi');
/*!40000 ALTER TABLE `BallArt` ENABLE KEYS */;


-- Dumping structure for table db_mini.Ballweg
CREATE TABLE IF NOT EXISTS `Ballweg` (
  `Anlage` int(11) NOT NULL,
  `Bahn` int(11) NOT NULL,
  `WegNummer` int(11) NOT NULL AUTO_INCREMENT,
  `Name` int(10) NOT NULL,
  `Kommentar` varchar(250) DEFAULT NULL,
  `BallArt` int(11) NOT NULL,
  PRIMARY KEY (`WegNummer`,`Anlage`,`Bahn`),
  KEY `fk_Ballwege_BallArt1_idx` (`BallArt`),
  KEY `fk_Ballwege_MinigolfBahn1` (`Anlage`,`Bahn`),
  KEY `fk_Ballwege_Userid` (`Name`),
  CONSTRAINT `fk_Ballwege_BallArt1` FOREIGN KEY (`BallArt`) REFERENCES `BallArt` (`BallId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Ballwege_MinigolfBahn1` FOREIGN KEY (`Anlage`, `Bahn`) REFERENCES `Bahn` (`Anlage`, `BahnNummer`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Ballwege_Userid` FOREIGN KEY (`Name`) REFERENCES `User` (`UserId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;


-- Dumping structure for table db_mini.Ballwegelinie
CREATE TABLE IF NOT EXISTS `Ballwegelinie` (
  `Anlage` int(11) NOT NULL,
  `Bahn` int(11) NOT NULL,
  `Ballweg` int(11) NOT NULL,
  `LinieNummer` int(11) NOT NULL AUTO_INCREMENT,
  `KoordX` int(11) NOT NULL,
  `KoordY` int(11) NOT NULL,
  PRIMARY KEY (`LinieNummer`,`Anlage`,`Bahn`,`Ballweg`),
  KEY `fk_Ballwegelinie_Ballweg1_idx` (`Ballweg`,`Anlage`,`Bahn`),
  CONSTRAINT `fk_Ballwegelinie_Ballweg1` FOREIGN KEY (`Ballweg`, `Anlage`, `Bahn`) REFERENCES `Ballweg` (`WegNummer`, `Anlage`, `Bahn`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;


-- Dumping structure for table db_mini.Kommentar
CREATE TABLE IF NOT EXISTS `Kommentar` (
  `Anlage` int(11) NOT NULL,
  `Bahn` int(11) NOT NULL,
  `Ballweg` int(11) NOT NULL,
  `KommentarNummer` int(11) NOT NULL AUTO_INCREMENT,
  `Kommentar` varchar(250) CHARACTER SET utf8 NOT NULL,
  `Autor` int(11) NOT NULL,
  `Zeit` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`KommentarNummer`,`Anlage`,`Bahn`,`Ballweg`),
  KEY `fk_Kommentar_User1_idx` (`Autor`),
  KEY `fk_Kommentar_Ballweg1` (`Ballweg`,`Anlage`,`Bahn`),
  CONSTRAINT `fk_Kommentar_Ballweg1` FOREIGN KEY (`Ballweg`, `Anlage`, `Bahn`) REFERENCES `Ballweg` (`WegNummer`, `Anlage`, `Bahn`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Kommentar_User1` FOREIGN KEY (`Autor`) REFERENCES `User` (`UserId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;



-- Dumping structure for table db_mini.Recht
CREATE TABLE IF NOT EXISTS `Recht` (
  `RechtId` int(11) NOT NULL AUTO_INCREMENT,
  `Typ` varchar(45) CHARACTER SET latin1 COLLATE latin1_german2_ci NOT NULL,
  PRIMARY KEY (`RechtId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table db_mini.Recht: ~3 rows (approximately)
/*!40000 ALTER TABLE `Recht` DISABLE KEYS */;
INSERT INTO `Recht` (`RechtId`, `Typ`) VALUES
	(1, 'Administrator'),
	(2, 'Moderator'),
	(3, 'User');
/*!40000 ALTER TABLE `Recht` ENABLE KEYS */;


-- Dumping structure for table db_mini.User
CREATE TABLE IF NOT EXISTS `User` (
  `UserId` int(11) NOT NULL AUTO_INCREMENT,
  `Username` varchar(45) CHARACTER SET latin1 COLLATE latin1_german2_ci NOT NULL,
  `Passwort` varchar(45) CHARACTER SET latin1 COLLATE latin1_german2_ci NOT NULL,
  `Vorname` varchar(45) CHARACTER SET latin1 COLLATE latin1_german2_ci DEFAULT NULL,
  `Nachname` varchar(45) CHARACTER SET latin1 COLLATE latin1_german2_ci DEFAULT NULL,
  PRIMARY KEY (`UserId`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;


-- Dumping structure for table db_mini.UserAnlagenRechte
CREATE TABLE IF NOT EXISTS `UserAnlagenRechte` (
  `User` int(11) NOT NULL,
  `Anlage` int(11) NOT NULL,
  `Rechte` int(11) NOT NULL,
  PRIMARY KEY (`User`,`Anlage`),
  KEY `fk_User_has_Anlage_Anlage1_idx` (`Anlage`),
  KEY `fk_User_has_Anlage_User1_idx` (`User`),
  KEY `fk_UserRechte_Recht1_idx` (`Rechte`),
  CONSTRAINT `fk_User_has_Anlage_Anlage1` FOREIGN KEY (`Anlage`) REFERENCES `Anlage` (`AnlageId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_UserRechte_Recht1` FOREIGN KEY (`Rechte`) REFERENCES `Recht` (`RechtId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_User_has_Anlage_User1` FOREIGN KEY (`User`) REFERENCES `User` (`UserId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


/*!40000 ALTER TABLE `UserAnlagenRechte` ENABLE KEYS */;
/*!40014 SET FOREIGN_KEY_CHECKS=1 */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
